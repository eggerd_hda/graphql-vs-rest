# GraphQL vs REST

Dies ist eine beispielhafte Implementierung eines simplen Backend-Servers, welche zum Vergleichen von GraphQL und REST verwendet wurde. Der Server stellt CRUD sowohl über eine GraphQL- als auch eine RESTful-API bereit.  

Die Implementierung wurde zu Vergleichszwecken so minimal wie möglich gehalten. Für die REST-API sind daher folgende Anmerkungen zu beachten: 
- unbekannte Ressourcen werden ignoriert und resultieren nicht in einer `404`-Antwort; es wird lediglich ein leeres Array zurückgegeben
- PUT/POST Endpunkte ignorieren fehlende Properties oder inkorrekte Datentypen
- zusammengehörige Ressourcen sind nicht durch Links miteinander verknüpft (HATEOAS)

#### Datenbank

![er-diagram](https://gitlab.com/eggerd_hda/graphql-vs-rest/-/wikis/uploads/er-diagram.png)

#### REST

Unter `http://localhost:3000/rest` stehen die REST-API mit den nachfolgenden Endpunkte zur Verfügung. Bei allen GET-Requests kann die Anzahl der zurückgegebenen Objekte durch den Query-Parameter `limit={number}` beschränkt werden.

##### Professoren

| Methode | Pfad | Anmerkung |
| --- | --- | --- |
| GET | `/professors` | 
| GET | `/professors/{id}` |
| POST | `/professors` | Erwartet ein Professor-Objekt wie aus den GET-Requests im Body |
| PUT | `/professors/{id}` | Erwartet ein Professor-Objekt wie aus den GET-Requests im Body |
| DELETE | `/professors/{id}` |
| GET | `/professors/{id}/courses` |

##### Kurse

| Methode | Pfad | Anmerkung |
| --- | --- | --- |
| GET | `/courses` |
| GET | `/courses/{id}` |
| POST | `/courses` | Erwartet ein Course-Objekt wie aus den GET-Requests im Body
| PUT | `/courses/{id}` | Erwartet ein Course-Objekt wie aus den GET-Requests im Body |
| DELETE | `/courses/{id}` |
| GET | `/courses/{id}/students` | 
| POST | `/courses/{id}/students` | Erwartet ein Objekt der Form `{ studentId: number }` im Body |
| DELETE | `/courses/{cId}/students/{sId}` |

##### Studenten

| Methode | Pfad | Anmerkung |
| --- | --- | --- |
| GET | `/students` |
| GET | `/students/{id}` |
| POST | `/students` | Erwartet ein Student-Objekt wie aus den GET-Requests im Body |
| PUT | `/students/{id}` | Erwartet ein Student-Objekt wie aus den GET-Requests im Body |
| DELETE | `/students/{id}` |
| GET | `/students/{id}/courses` |

#### GraphQL

Unter `http://localhost:3000/graphql` steht die GraphQL-API mit dem nachfolgenden Schema zur Verfügung. Wird der Endpunkt im Browser aufgerufen, wird ein interaktiver IDE zum Erstellen und Ausführen von Queries bereitgestellt.

```graphql
enum Department {
  Mathematik
  Informatik
  Maschinenbau
  Chemie
}

type Student {
  id: Int!
  title: String!
  forename: String!
  surname: String!
  address: String!
  department: Department!
  semester: Int!
  enrollment: Int!
  courses(limit: Int): [Course!]!
}

input StudentInput {
  title: String!
  forename: String!
  surname: String!
  address: String!
  department: Department!
  semester: Int!
  enrollment: Int!
}

type Professor {
  id: Int!
  title: String!
  forename: String!
  surname: String!
  department: Department!
  phone: String!
  room: Int!
  officeHours: String!
  courses(limit: Int): [Course!]!
}

input ProfessorInput {
  title: String!
  forename: String!
  surname: String!
  department: Department!
  phone: String!
  room: Int!
  officeHours: String!
}

type Course {
  id: Int!
  name: String!
  professor: Professor!
  semester: Int!
  room: Int!
  time: String!
  students(limit: Int): [Student!]!
}

input CourseInput {
  name: String!
  professorId: Int!
  semester: Int!
  room: Int!
  time: String!
}

# note that the put-mutations are used for creating and updating!
type Mutation {
  putStudent(id: Int, student: StudentInput!): [Student!]!        
  deleteStudent(id: Int!): Boolean!
  putProfessor(id: Int, professor: ProfessorInput!): [Professor!]!
  deleteProfessor(id: Int!): Boolean!
  putCourse(id: Int, course: CourseInput!): [Course!]!
  deleteCourse(id: Int!): Boolean!
  postCourseStudent(courseId: Int!, studentId: Int!): Boolean!    
  deleteCourseStudent(courseId: Int!, studentId: Int!): Boolean!  
}

type Query {
  students(id: Int, limit: Int): [Student!]!
  professors(id: Int, limit: Int): [Professor!]!
  courses(id: Int, limit: Int): [Course!]!
}

schema {
  query: Query
  mutation: Mutation
}
```

## Anforderungen

- [Node.js](https://nodejs.org/) (v12.18) inkl. `npm`
- [MariaDB](https://mariadb.com/) (v10.1) (oder vergleichbar)
- 4 GB freien Arbeitsspeicher, falls Tests ausgeführt werden sollen

## Installation

1. Datenbank mit dem Schema aus `/src/db/schema.sql` anlegen
1. Zugangsdaten für die Datenbank in `.env` eintragen
1. `npm install` ausführen um alle Abhängigkeiten zu installieren
1. `npm run seed` ausführen um die Datenbank mit Testdatensätzen zu füllen (alternativ [test-seed.sql](https://gitlab.com/eggerd_hda/graphql-vs-rest/-/wikis/uploads/test-seed.sql) importieren)

## Running It

- `npm run start` startet den Backend-Server
- `npm run test` führt die GraphQL und REST Performance-Tests aus
- `npm run test:gql` führt nur die GraphQL Performance-Tests aus
- `npm run test:rest` führt nur die REST Performance-Tests aus

**Hinweis:** Zum Ausführen der Tests muss zunächst der Backend-Server gestartet werden!
