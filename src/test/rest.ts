import request, { RequestPromise } from 'request-promise';
import { getMeasurements, runTests, Test } from './test';
import { getProfessorCourses } from '../rest/professors.controller';

const baseUrl = `${process.env.TEST_BASE_URL}/rest`;

/**
 * Get all students with all fields
 */
const getAllStudentsFull: Test = async options => {
  return getMeasurements(await request.get(`${baseUrl}/students`, options));
}

/**
 * Get all courses with all fields
 */
const getAllCoursesFull: Test = async options => {
  return getMeasurements(await request.get(`${baseUrl}/courses`, options));
}

/**
 * Get all professors with all fields
 */
const getAllProfessorsFull: Test = async options => {
  return getMeasurements(await request.get(`${baseUrl}/professors`, options));
}

/**
 * Get only 1 student with all fields
 */
const get1StudentFull: Test = async options => {
  return getMeasurements(await request.get(`${baseUrl}/students`, { ...options, qs: { limit: 1 }}));
}

/**
 * Get only 10 students with all fields
 */
const get10StudentsFull: Test = async options => {
  return getMeasurements(await request.get(`${baseUrl}/students`, { ...options, qs: { limit: 10 }}));
}

/**
 * Get only 100 students with all fields
 */
const get100StudentsFull: Test = async options => {
  return getMeasurements(await request.get(`${baseUrl}/students`, { ...options, qs: { limit: 100 }}));
}

/**
 * Get only 1000 students with all fields
 */
const get1000StudentsFull: Test = async options => {
  return getMeasurements(await request.get(`${baseUrl}/students`, { ...options, qs: { limit: 1000 }}));
}

/**
 * Get only 10.000 students with all fields
 */
const get10000StudentsFull: Test = async options => {
  return getMeasurements(await request.get(`${baseUrl}/students`, { ...options, qs: { limit: 10000 }}));
}

/**
 * Get only 10.000 students, including all their courses
 * NOTE: This tests time measurement includes request queue times as well as body parse times
 */
const get10000StudentsFullWithCoursesFull: Test = async options => {
  const startTime = Date.now();
  const studentsReq = await request.get(`${baseUrl}/students`, { ...options, qs: { limit: 10000 }});
  const measurements = getMeasurements(studentsReq);

  const courseSubRequests: Promise<any>[] = [];
  for (const student of JSON.parse(studentsReq.body)) { // get all courses for each student
    courseSubRequests.push(new Promise(resolve => {
      request.get(`${baseUrl}/students/${student.id}/courses`, options).then(response => {
        measurements.size += getMeasurements(response).size;
        JSON.parse(response.body); // just to include it in the time measurement
        resolve();
      });
    }));
  }

  await Promise.all(courseSubRequests); // wait until all sub requests & callbacks have finished
  measurements.time += Date.now() - startTime;

  return measurements;
}

/**
 * Get only 5.000 students, including all their courses and the prof of each course
 * NOTE: This tests time measurement includes request queue times as well as body parse times
 */
const get5000StudentsFullWithCoursesFullWithProfsFull: Test = async options => {
  const startTime = Date.now();
  const studentsReq = await request.get(`${baseUrl}/students`, { ...options, qs: { limit: 5000 }});
  const measurements = getMeasurements(studentsReq);

  const courseSubRequests: Promise<any>[] = [];
  const professorSubRequests: Promise<any>[] = [];

  /** helper function that loads a single professor */
  const getProfessor = (profId: Number) => new Promise(resolve => {
    request.get(`${baseUrl}/professors/${profId}`, options).then(response => {
      measurements.size += getMeasurements(response).size;
      JSON.parse(response.body); // just to include it in the time measurement
      resolve();
    });
  })

  // get all courses for each student
  for (const student of JSON.parse(studentsReq.body)) {
    courseSubRequests.push(new Promise(resolve => {
      request.get(`${baseUrl}/students/${student.id}/courses`, options).then(async response => {
        measurements.size += getMeasurements(response).size;

        // get the professor for each course of a student
        for (const course of JSON.parse(response.body)) {
          professorSubRequests.push(getProfessor(course.professorId));
        }

        await Promise.all(professorSubRequests); // wait until all profs have been loaded
        resolve();
      });
    }));
  }

  await Promise.all(courseSubRequests); // wait until all sub requests & callbacks have finished
  measurements.time += Date.now() - startTime;

  return measurements;
}

console.log(`testing rest (${process.env.TEST_REPETITIONS} repetitions each)`);
runTests({
  get1StudentFull,
  get10StudentsFull,
  get100StudentsFull,
  get1000StudentsFull,
  get10000StudentsFull,
  get10000StudentsFullWithCoursesFull,
  get5000StudentsFullWithCoursesFullWithProfsFull,
  getAllProfessorsFull,
  getAllCoursesFull,
  getAllStudentsFull,

  // not possible with REST
  // getAllStudentsPartial,
  // getAllCoursesPartial,
  // getAllProfessorsPartial,
  // get1StudentFullWithCoursesFull,
  // get1StudentFullWithCoursesFullLooping1,
  // get1StudentFullWithCoursesFullLooping2,
});
