import request from 'request-promise';
import { getMeasurements, runTests, Test } from './test';

const baseUrl = `${process.env.TEST_BASE_URL}/graphql`;

/**
 * Get all students with all fields
 */
const getAllStudentsFull: Test = async options => {
  return getMeasurements(await request.post(baseUrl, { ...options, body: JSON.stringify({
    query: '{ students { id title forename surname address department semester enrollment }}',
  })}));
}

/**
 * Get all courses with all fields
 */
const getAllCoursesFull: Test = async options => {
  return getMeasurements(await request.post(baseUrl, { ...options, body: JSON.stringify({
    query: '{ courses { id name semester room time }}',
  })}));
}

/**
 * Get all professors with all fields
 */
const getAllProfessorsFull: Test = async options => {
  return getMeasurements(await request.post(baseUrl, { ...options, body: JSON.stringify({
    query: '{ professors { id title forename surname department phone room officeHours }}',
  })}));
}

/**
 * Get only 1 student with all fields
 */
const get1StudentFull: Test = async options => {
  return getMeasurements(await request.post(baseUrl, { ...options, body: JSON.stringify({
    query: '{ students(limit: 1) { id title forename surname address department semester enrollment }}',
  })}));
}

/**
 * Get only 10 students with all fields
 */
const get10StudentsFull: Test = async options => {
  return getMeasurements(await request.post(baseUrl, { ...options, body: JSON.stringify({
    query: '{ students(limit: 10) { id title forename surname address department semester enrollment }}',
  })}));
}

/**
 * Get only 100 students with all fields
 */
const get100StudentsFull: Test = async options => {
  return getMeasurements(await request.post(baseUrl, { ...options, body: JSON.stringify({
    query: '{ students(limit: 100) { id title forename surname address department semester enrollment }}',
  })}));
}

/**
 * Get only 1000 students with all fields
 */
const get1000StudentsFull: Test = async options => {
  return getMeasurements(await request.post(baseUrl, { ...options, body: JSON.stringify({
    query: '{ students(limit: 1000) { id title forename surname address department semester enrollment }}',
  })}));
}

/**
 * Get only 10.000 students with all fields
 */
const get10000StudentsFull: Test = async options => {
  return getMeasurements(await request.post(baseUrl, { ...options, body: JSON.stringify({
    query: '{ students(limit: 10000) { id title forename surname address department semester enrollment }}',
  })}));
}

/**
 * Get all students but only the id, forename & surname fields
 */
const getAllStudentsPartial: Test = async options => {
  return getMeasurements(await request.post(baseUrl, { ...options, body: JSON.stringify({
    query: '{ students { id forename surname }}',
  })}));
}

/**
 * Get all courses but only the id, name & title fields
 */
const getAllCoursesPartial: Test = async options => {
  return getMeasurements(await request.post(baseUrl, { ...options, body: JSON.stringify({
    query: '{ courses { id name time }}',
  })}));
}

/**
 * Get all professors but only the id, title, forname & surname fields
 */
const getAllProfessorsPartial: Test = async options => {
  return getMeasurements(await request.post(baseUrl, { ...options, body: JSON.stringify({
    query: '{ professors { id title forename surname }}',
  })}));
}

/**
 * Get only 10.000 students, including all their courses
 * NOTE: This tests time measurement includes request queue time as well as body parse time
 */
const get10000StudentsFullWithCoursesFull: Test = async options => {
  const startTime = Date.now();
  const response = await request.post(baseUrl, { ...options, body: JSON.stringify({
    query: `{ students(limit: 10000) {
      id title forename surname address department semester enrollment courses {
        id name semester room time
      }
    }}`,
  })});

  // this is only done because body parsing has to be done during the equivalent REST test
  JSON.parse(response.body);

  const measurements = getMeasurements(response);
  measurements.time = Date.now() - startTime;
  return measurements
}

/**
 * Get only 5.000 students, including all their courses and the prof of each course
 * NOTE: This tests time measurement includes request queue time as well as body parse time
 */
const get5000StudentsFullWithCoursesFullWithProfsFull: Test = async options => {
  const startTime = Date.now();
  const response = await request.post(baseUrl, { ...options, body: JSON.stringify({
    query: `{ students(limit: 5000) {
      id title forename surname address department semester enrollment courses {
        id name semester room time professor {
          id title forename surname department phone room officeHours
        }
      }
    }}`,
  })});

  // this is only done because body parsing has to be done during the equivalent REST test
  JSON.parse(response.body);

  const measurements = getMeasurements(response);
  measurements.time = Date.now() - startTime;
  return measurements
}

/**
 * Get only 1 student, including all their courses and all students of each course
 */
const get1StudentFullWithCoursesFull: Test = async options => {
  return getMeasurements(await request.post(baseUrl, { ...options, body: JSON.stringify({
    query: `{ students(limit: 1) {
      id title forename surname address department semester enrollment courses {
        id name semester room time
      }
    }}`,
  })}));
}

/**
 * Get only 1 student, including all their courses and all students of each course (looping 1x)
 */
const get1StudentFullWithCoursesFullLooping1: Test = async options => {
  return getMeasurements(await request.post(baseUrl, { ...options, body: JSON.stringify({
    query: `{ students(limit: 1) {
      id title forename surname address department semester enrollment courses {
        id name semester room time students {
          id title forename surname address department semester enrollment courses {
            id name semester room time
          }
        }
      }
    }}`,
  })}));
}

/**
 * Get only 1 student, including all their courses and all students of each course (looping 2x).
 * Note that one more loop or a limit > 3 will already cause the socket to hang up!
 */
const get1StudentFullWithCoursesFullLooping2: Test = async options => {
  return getMeasurements(await request.post(baseUrl, { ...options, body: JSON.stringify({
    query: `{ students(limit: 1) {
      id title forename surname address department semester enrollment courses {
        id name semester room time students {
          id title forename surname address department semester enrollment courses {
            id name semester room time students {
              id title forename surname address department semester enrollment courses {
                id name semester room time
              }
            }
          }
        }
      }
    }}`,
  })}));
}

console.log(`testing graphql (${process.env.TEST_REPETITIONS} repetitions each)`);
runTests({
  get1StudentFull,
  get10StudentsFull,
  get100StudentsFull,
  get1000StudentsFull,
  get10000StudentsFull,
  get10000StudentsFullWithCoursesFull,
  get5000StudentsFullWithCoursesFullWithProfsFull,
  getAllProfessorsFull,
  getAllCoursesFull,
  getAllStudentsFull,
  getAllStudentsPartial,
  getAllCoursesPartial,
  getAllProfessorsPartial,
  get1StudentFullWithCoursesFull,
  get1StudentFullWithCoursesFullLooping1,
  get1StudentFullWithCoursesFullLooping2,
});
