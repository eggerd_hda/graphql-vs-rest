import request, { RequestPromiseOptions } from 'request-promise';

/**
 * Options that will be used for all requests
 */
const options: RequestPromiseOptions = {
  time: true, // enable various time measurements
  resolveWithFullResponse: true, // otherwise only the body is returned
  headers: { 'content-type': 'application/json' },
  timeout: 180000,
}

/**
 * Measurements that each test should provide
 */
interface Measurements {
  time: number // in milliseconds
  size: number // in bytes
}

/**
 * Implements a test case and returns a set of measurements (e.g. elapsed time)
 * @param options a set of global options that should be used for requests
 */
export type Test = (options: RequestPromiseOptions) => Promise<Measurements>;

/**
 * Runs all passed tests sequentially and collects various values based on the returned measurements.
 * Each test is repeated for TEST_REPETITIONS times
 */
export const runTests = async (testQueue: {[name: string]: Test}) => {
  await serverWakeUp();
  console.time('test duration');

  const repetitions = Number(process.env.TEST_REPETITIONS);
  const queueResults: any = {};
  for (const test of Object.keys(testQueue)) { // for each test in the queue
    console.log(`running '${test}'`);

    const cache = { timeMin: 0, timeAvg: 0, timeMax: 0, sizeAvg: 0 };
    for (let i = 0; i < repetitions; i++) { // repeating the test
      const res = await testQueue[test](options);
      cache.timeAvg += res.time;
      cache.timeMax = res.time > cache.timeMax ? res.time : cache.timeMax;
      cache.timeMin = !cache.timeMin || res.time < cache.timeMin ? res.time : cache.timeMin;
      cache.sizeAvg += res.size;
    }

    cache.timeAvg = cache.timeAvg / repetitions;
    cache.sizeAvg = cache.sizeAvg / repetitions;
    queueResults[test] = cache;
  }

  console.table(queueResults);
  console.timeEnd('test duration');
}

/**
 * Extracts and converts the required measurements from a response object
 */
export const getMeasurements = (res: any) => {
  const measurements = { time: res.timings.end, size: Number(res.headers['content-length']) };
  if (process.env.DEBUG_PRINT_MEASUREMENTS === 'true') console.log(measurements);
  return measurements;
}

/**
 * This is used to wake up the server process before running the actual tests, because otherwise the
 * very first request to the server will take considerably more time than all other requests (like
 * up to 100ms). So this just requests the 'hello world' some times, to ensure the server is ready
 */
export const serverWakeUp = async () => {
  for (let i = 0; i < 100; i++) {
    await request.get(`${process.env.TEST_BASE_URL}`, options);
  }
}
