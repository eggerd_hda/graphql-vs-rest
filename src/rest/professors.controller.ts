import { Request, Response } from 'express';
import { toSqlInsert } from '../db/to-sql-insert';

/**
 * Create a new professor or update an existing one (params.id)
 */
export const putProfessor = async (req: Request, res: Response) => {
  const professor = {
    id: req.params?.id, // only set on 'put', not on 'post'
    title: req.body?.title,
    forename: req.body?.forename,
    surname: req.body?.surname,
    department: req.body?.department,
    phone: req.body?.phone,
    room: req.body?.room,
    officeHours: req.body?.officeHours,
  };

  // remove all properties with undefined values
  Object.keys(professor).forEach(k => !(professor as any)[k] && delete (professor as any)[k]);

  const insert = toSqlInsert([professor]);
  const result = await req.app.get('db').query(`replace into professors (${insert.columns}) values ${insert.values}`);
  res.status(201).send(await req.app.get('db').query(`select * from professors where id = ${result.insertId}`));
}

/**
 * Get all or a single professor (params.id). Response size can be limited (query.limit)
 */
export const getProfessors = async (req: Request, res: Response) => {
  const where = req.params.id ? `where id = ${req.params.id}` : ``;
  const limit = req.query.limit ? `limit ${req.query.limit}` : ``;
  res.send(await req.app.get('db').query(`select * from professors ${where} ${limit}`));
}

/**
 * Delete a professor by ID (params.id)
 */
export const deleteProfessor = async (req: Request, res: Response) => {
  await req.app.get('db').query(`delete from professors where id = ${req.params.id}`);
  res.sendStatus(204);
}

/**
 * Get all courses of a professor (params.id). Response size can be limited (query.limit)
 */
export const getProfessorCourses = async (req: Request, res: Response) => {
  const limit = req.query.limit ? `limit ${req.query.limit}` : ``;
  res.send(await req.app.get('db').query(`select * from courses where professorId = ${req.params.id} ${limit}`));
}
