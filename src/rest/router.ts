import express from 'express';
import { deleteCourse, deleteCourseStudent, getCourses, getCourseStudents, postCourseStudent, putCourse } from './courses.controller';
import { deleteProfessor, getProfessorCourses, getProfessors, putProfessor } from './professors.controller';
import { deleteStudent, getStudentCourses, getStudents, putStudent } from './students.controller';

export const restRouter: express.Router = express.Router();

/**
 * Professor routes
 */
const professorRouter: express.Router = express.Router();
restRouter.use('/professors', professorRouter);

professorRouter.post('/', putProfessor);
professorRouter.put('/:id', putProfessor);
professorRouter.get('/:id?', getProfessors);
professorRouter.delete('/:id', deleteProfessor);

professorRouter.get('/:id/courses', getProfessorCourses);

/**
 * Course routes
 */
const courseRouter: express.Router = express.Router();
restRouter.use('/courses', courseRouter);

courseRouter.post('/', putCourse);
courseRouter.put('/:id', putCourse);
courseRouter.get('/:id?', getCourses);
courseRouter.delete('/:id', deleteCourse);

courseRouter.post('/:id/students', postCourseStudent);
courseRouter.get('/:id/students', getCourseStudents);
courseRouter.delete('/:cId/students/:sId', deleteCourseStudent);

/**
 * Student routes
 */
const studentRouter: express.Router = express.Router();
restRouter.use('/students', studentRouter);

studentRouter.post('/', putStudent);
studentRouter.put('/:id', putStudent);
studentRouter.get('/:id?', getStudents);
studentRouter.delete('/:id', deleteStudent);

studentRouter.get('/:id/courses', getStudentCourses);
