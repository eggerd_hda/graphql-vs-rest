import { Request, Response } from 'express';
import { toSqlInsert } from '../db/to-sql-insert';

/**
 * Create a new course or update an existing one (params.id)
 */
export const putCourse = async (req: Request, res: Response) => {
  const course = {
    id: req.params?.id, // only set on 'put', not on 'post'
    name: req.body?.name,
    professor: req.body?.professor,
    semester: req.body?.semester,
    room: req.body?.room,
    time: req.body?.time,
  };

  // remove all properties with undefined values
  Object.keys(course).forEach(k => !(course as any)[k] && delete (course as any)[k]);

  const insert = toSqlInsert([course]);
  const result = await req.app.get('db').query(`replace into courses (${insert.columns}) values ${insert.values}`);
  res.status(201).send(await req.app.get('db').query(`select * from courses where id = ${result.insertId}`));
}

/**
 * Get all or a single course (params.id). Response size can be limited (query.limit)
 */
export const getCourses = async (req: Request, res: Response) => {
  const where = req.params.id ? `where id = ${req.params.id}` : ``;
  const limit = req.query.limit ? `limit ${req.query.limit}` : ``;
  res.send(await req.app.get('db').query(`select * from courses ${where} ${limit}`));
}

/**
 * Delete a course by ID (params.id)
 */
export const deleteCourse = async (req: Request, res: Response) => {
  await req.app.get('db').query(`delete from courses where id = ${req.params.id}`);
  res.sendStatus(204);
}

/**
 * Add a student (body.studentId) to a course (params.id)
 */
export const postCourseStudent = async (req: Request, res: Response) => {
  await req.app.get('db').query(`replace into join_students_courses (courseId, studentId)
    values (${req.params.id}, ${req.body.studentId})`);
  res.sendStatus(204);
}

/**
 * Get all the students of a course (params.id). Response size can be limited (query.limit)
 */
export const getCourseStudents = async (req: Request, res: Response) => {
  const limit = req.query.limit ? `limit ${req.query.limit}` : ``;
  res.send(await req.app.get('db').query(`select * from students as s left join join_students_courses
    as jsc on s.id = jsc.studentId where courseId = ${req.params.id} ${limit}`));
}

/**
 * Delete a student (params.sId) from a course (params.cId)
 */
export const deleteCourseStudent = async (req: Request, res: Response) => {
  await req.app.get('db').query(`delete from join_students_courses where studentId = ${req.params.sId}
    && courseId = ${req.params.cId}`);
  res.sendStatus(204);
}
