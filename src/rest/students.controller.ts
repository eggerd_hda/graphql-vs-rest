import { Request, Response } from 'express';
import { toSqlInsert } from '../db/to-sql-insert';

/**
 * Create a new student or update an existing one (params.id)
 */
export const putStudent = async (req: Request, res: Response) => {
  const student = {
    id: req.params?.id, // only set on 'put', not on 'post'
    title: req.body?.title,
    forename: req.body?.forename,
    surname: req.body?.surname,
    address: req.body?.address,
    department: req.body?.department,
    semester: req.body?.semester,
    enrollment: req.body?.enrollment,
  };

  // remove all properties with undefined values
  Object.keys(student).forEach(k => !(student as any)[k] && delete (student as any)[k]);

  const insert = toSqlInsert([student]);
  const result = await req.app.get('db').query(`replace into students (${insert.columns}) values ${insert.values}`);
  res.status(201).send(await req.app.get('db').query(`select * from students where id = ${result.insertId}`));
}

/**
 * Get all or a single student (params.id). Response size can be limited (query.limit)
 */
export const getStudents = async (req: Request, res: Response) => {
  const where = req.params.id ? `where id = ${req.params.id}` : ``;
  const limit = req.query.limit ? `limit ${req.query.limit}` : ``;
  res.send(await req.app.get('db').query(`select * from students ${where} ${limit}`));
}

/**
 * Delete a student by ID (params.id)
 */
export const deleteStudent = async (req: Request, res: Response) => {
  await req.app.get('db').query(`delete from students where id = ${req.params.id}`);
  res.sendStatus(204);
}

/**
 * Get all courses of a student (params.id). Response size can be limited (query.limit)
 */
export const getStudentCourses = async (req: Request, res: Response) => {
  const limit = req.query.limit ? `limit ${req.query.limit}` : ``;
  res.send(await req.app.get('db').query(`select * from courses as c left join join_students_courses
    as jsc on c.id = jsc.courseId where studentId = ${req.params.id} ${limit}`));
}
