import bodyParser from 'body-parser';
import express from 'express';
import graphqlHTTP from 'express-graphql';
import { dbConnect } from './db/connect';
import { schema } from './graphql/root';
import { restRouter } from './rest/router';

const bootstrap = async () => {
  const app = express();
  const db = await dbConnect();

  // connect to the database and make it available in the request context
  app.set('db', db);

  // just for verifying the server is listening
  app.get('/', (req, res) => res.send('Hello World!'));

  // bind all routes of the rest api
  app.use('/rest', bodyParser.json(), restRouter);

  // bind the graphql server to a route
  app.use('/graphql', graphqlHTTP({ schema, context: { db }, graphiql: true }))

  // start the express server
  app.listen(3000, () => console.log('test server is listening on port 3000!'));
}

bootstrap();
