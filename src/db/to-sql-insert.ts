/**
 * Get the columns (based on object properties) and values for use in an SQL insert query
 */
export const toSqlInsert = (data: object[]) => {
  const values: string[] = [];
  for (const entry of data) {
    values.push(Object.values(entry).map(v => `'${v}'`).join(','));
  }

  return {
    columns: Object.keys(data[0]).join(','),
    values: values.map(v => `(${v})`).join(','),
    amount: data.length - 1,
  };
}
