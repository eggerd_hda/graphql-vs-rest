import mysql from 'promise-mysql';

/**
 * Initialize a database connection
 */
export const dbConnect = async (): Promise<mysql.Connection> => {
  return mysql.createConnection({
    // debug: [ 'ComQueryPacket' ],
    multipleStatements: true,
    host: process.env.DB_HOSTNAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
  });
}
