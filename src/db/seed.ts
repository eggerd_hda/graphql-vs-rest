import faker from 'faker/locale/de';
import { dbConnect } from './connect';
import { toSqlInsert } from './to-sql-insert';

// some values that are not available via faker
const departments = [ 'Mathematik', 'Informatik', 'Maschinenbau', 'Chemie' ];
const hours = [ '8:30 - 10:00', '10:15 - 11:45', '12:00 - 13:30', '14:15 - 15:45', '16:00 - 17:30' ];

/**
 * Generate random data for the 'students' table
 */
const generateStudents = () => {
  console.log(`generating students (${process.env.SEEDER_AMOUNT_STUDENTS})`);

  const students: any[] = [];
  for (let i = 0; i < Number(process.env.SEEDER_AMOUNT_STUDENTS); i++) {
    students.push({
      title: faker.name.prefix(),
      forename: faker.name.firstName(),
      surname: faker.name.lastName(),
      address: `${faker.address.streetAddress()}, ${faker.address.zipCode()} ${faker.address.city()}`,
      department: departments[faker.random.number(departments.length - 1)],
      semester: faker.random.number({ min: 1, max: 9 }),
      enrollment: new Date(faker.date.past(10).toISOString()).getTime() / 1000,
    });
  }

  return toSqlInsert(students);
}

/**
 * Generate random data for the 'courses' table
 * @param maxProfId max ID value that is used for the foreign key 'professor'
 */
const generateCourses = (maxProfId: number) => {
  console.log(`generating courses (${process.env.SEEDER_AMOUNT_COURSES})`);

  const courses: any[] = [];
  for (let i = 0; i < Number(process.env.SEEDER_AMOUNT_COURSES); i++) {
    courses.push({
      name: faker.company.catchPhrase(),
      professorId: faker.random.number({ min: 1, max: maxProfId }),
      semester: faker.random.number({ min: 1, max: 9 }),
      room: faker.random.number({ min: 100, max: 800 }),
      time: hours[faker.random.number(hours.length - 1)],
    });
  }

  return toSqlInsert(courses);
}

/**
 * Generate random data for the 'professors' table
 */
const generateProfessors = () => {
  console.log(`generating professors (${process.env.SEEDER_AMOUNT_PROFS})`);

  const professors: any[] = [];
  for (let i = 0; i < Number(process.env.SEEDER_AMOUNT_PROFS); i++) {
    professors.push({
      title: faker.name.prefix(),
      forename: faker.name.firstName(),
      surname: faker.name.lastName(),
      department: departments[faker.random.number(departments.length - 1)],
      phone: faker.phone.phoneNumber(),
      room: faker.random.number({ min: 100, max: 800 }),
      officeHours: hours[faker.random.number(departments.length - 1)],
    });
  }

  return toSqlInsert(professors);
}

/**
 * Generate random data for the join table of 'students' and 'courses'
 * @param maxStudentId max ID value that is used for the foreign key 'studentId'
 * @param maxCourseId max ID value that is used for the foreign key 'courseId'
 */
const generateStudentCourseJoins = (maxStudentId: number, maxCourseId: number) => {
  console.log(`generating student course joins (max ${process.env.SEEDER_MAX_STUDENT_COURSES} each)`);

  const assignments: any[] = [];
  for (let studentId = 1; studentId <= maxStudentId; studentId++) {
    const used: any[] = [];
    for (let i = 0; i < faker.random.number(Number(process.env.SEEDER_MAX_STUDENT_COURSES)); i++) {
      const courseId = faker.random.number({ min: 1, max: maxCourseId });
      if (!used.includes(courseId)) {
        assignments.push({ studentId, courseId });
        used.push(courseId);
      }
    }
  }

  return toSqlInsert(assignments);
}

/**
 * Seed all the database tables with randomly generated data
 */
const seed = async () => {
  console.log('seeding database');
  console.time('seeding duration');

  // generate data for all tables
  const professors = generateProfessors();
  const courses = generateCourses(professors.amount);
  const students = generateStudents();
  const joins = generateStudentCourseJoins(students.amount, courses.amount);

  // clearing database and inserting generated data
  console.log('inserting data');
  const db = await dbConnect();
  db.beginTransaction();
  db.query('truncate professors; truncate courses; truncate students; truncate join_students_courses');
  db.query(`insert into professors (${professors.columns}) values ${professors.values}`);
  db.query(`insert into courses (${courses.columns}) values ${courses.values}`);
  db.query(`insert into students (${students.columns}) values ${students.values}`);
  db.query(`insert into join_students_courses (${joins.columns}) values ${joins.values}`);
  await db.commit();

  console.timeEnd('seeding duration');
  db.end();
}

seed();
