export const studentType = `
  type Student {
    id: Int!
    title: String!
    forename: String!
    surname: String!
    address: String!
    department: Department!
    semester: Int!
    enrollment: Int!
    courses(limit: Int): [Course!]!
  }

  input StudentInput {
    title: String!
    forename: String!
    surname: String!
    address: String!
    department: Department!
    semester: Int!
    enrollment: Int!
  }

  type Query {
    students(id: Int, limit: Int): [Student!]!
  }

  type Mutation {
    putStudent(id: Int, student: StudentInput!): [Student!]!
    deleteStudent(id: Int!): Boolean!
  }
`;
