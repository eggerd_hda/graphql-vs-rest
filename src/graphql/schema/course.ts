export const courseType = `
  type Course {
    id: Int!
    name: String!
    professor: Professor!
    semester: Int!
    room: Int!
    time: String!
    students(limit: Int): [Student!]!
  }

  input CourseInput {
    name: String!
    professorId: Int!
    semester: Int!
    room: Int!
    time: String!
  }

  type Query {
    courses(id: Int, limit: Int): [Course!]!
  }

  type Mutation {
    putCourse(id: Int, course: CourseInput!): [Course!]!
    deleteCourse(id: Int!): Boolean!
    postCourseStudent(courseId: Int!, studentId: Int!): Boolean!
    deleteCourseStudent(courseId: Int!, studentId: Int!): Boolean!
  }
`;
