export const professorType = `
  type Professor {
    id: Int!
    title: String!
    forename: String!
    surname: String!
    department: Department!
    phone: String!
    room: Int!
    officeHours: String!
    courses(limit: Int): [Course!]!
  }

  input ProfessorInput {
    title: String!
    forename: String!
    surname: String!
    department: Department!
    phone: String!
    room: Int!
    officeHours: String!
  }

  type Query {
    professors(id: Int, limit: Int): [Professor!]!
  }

  type Mutation {
    putProfessor(id: Int, professor: ProfessorInput!): [Professor!]!
    deleteProfessor(id: Int!): Boolean!
  }
`;
