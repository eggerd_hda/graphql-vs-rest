import { toSqlInsert } from '../../db/to-sql-insert';

/**
 * Create a new student or update an existing one (args.id)
 */
const putStudent = async (obj: any, args: any, context: any) => {
  const insert = toSqlInsert([{ id: args.id, ...args.student}]); // id can be undefined
  const result = await context.db.query(`replace into students (${insert.columns}) values ${insert.values}`);
  return context.db.query(`select * from students where id = ${result.insertId}`);
}

/**
 * Get all or a single student (args.id). Response size can be limited (args.limit)
 */
const getStudents = (obj: any, args: any, context: any) => {
  const where = args.id ? `where id = ${args.id}` : ``;
  const limit = args.limit ? `limit ${args.limit}` : ``;
  return context.db.query(`select * from students ${where} ${limit}`);
}

/**
 * Delete a student by ID (args.id)
 */
const deleteStudent = async (obj: any, args: any, context: any) => {
  await context.db.query(`delete from students where id = ${args.id}`);
  return true;
}

/**
 * Get all courses of a student (obj.id). Response size can be limited (args.limit)
 */
const getStudentCourses = (obj: any, args: any, context: any) => {
  const limit = args.limit ? `limit ${args.limit}` : ``;
  return context.db.query(`select * from courses as c left join join_students_courses as jsc on
    c.id = jsc.courseId where studentId = ${obj.id} ${limit}`);
}

export const studentResolvers = {
  Student: {
    courses: getStudentCourses,
  },

  Query: {
    students: getStudents,
  },

  Mutation: {
    putStudent,
    deleteStudent,
  },
};
