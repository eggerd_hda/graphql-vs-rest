import { toSqlInsert } from '../../db/to-sql-insert';

/**
 * Create a new professor or update an existing one (args.id)
 */
const putProfessor = async (obj: any, args: any, context: any) => {
  const insert = toSqlInsert([{ id: args.id, ...args.professor}]); // id can be undefined
  const result = await context.db.query(`replace into professors (${insert.columns}) values ${insert.values}`);
  return context.db.query(`select * from professors where id = ${result.insertId}`);
}

/**
 * Get all or a single professor (args.id). Response size can be limited (args.limit)
 */
export const getProfessors = (obj: any, args: any, context: any) => {
  const where = args.id ? `where id = ${args.id}` : ``;
  const limit = args.limit ? `limit ${args.limit}` : ``;
  return context.db.query(`select * from professors ${where} ${limit}`);
}

/**
 * Delete a professor by ID (args.id)
 */
const deleteProfessor = async (obj: any, args: any, context: any) => {
  await context.db.query(`delete from professors where id = ${args.id}`);
  return true;
}

/**
 * Get all courses of a professor (obj.id). Response size can be limited (args.limit)
 */
const getProfessorCourses = (obj: any, args: any, context: any) => {
  const limit = args.limit ? `limit ${args.limit}` : ``;
  return context.db.query(`select * from courses where professorId = ${obj.id} ${limit}`);
}

export const professorResolvers = {
  Professor: {
    courses: getProfessorCourses,
  },

  Query: {
    professors: getProfessors,
  },

  Mutation: {
    putProfessor,
    deleteProfessor,
  },
};
