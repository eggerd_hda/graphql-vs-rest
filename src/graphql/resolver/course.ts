import { toSqlInsert } from '../../db/to-sql-insert';
import { getProfessors } from './professor';

/**
 * Create a new course or update an existing one (args.id)
 */
const putCourse = async (obj: any, args: any, context: any) => {
  const insert = toSqlInsert([{ id: args.id, ...args.course }]); // id can be undefined
  const result = await context.db.query(`replace into courses (${insert.columns}) values ${insert.values}`);
  return context.db.query(`select * from courses where id = ${result.insertId}`);
}

/**
 * Get all or a single course (obj.id). Response size can be limited (args.limit)
 */
const getCourses = (obj: any, args: any, context: any) => {
  const where = args.id ? `where id = ${args.id}` : ``;
  const limit = args.limit ? `limit ${args.limit}` : ``;
  return context.db.query(`select * from courses ${where} ${limit}`);
}

/**
 * Delete a course by ID (args.id)
 */
const deleteCourse = async (obj: any, args: any, context: any) => {
  await context.db.query(`delete from courses where id = ${args.id}`);
  return true;
}

/**
 * Add a student (args.studentId) to a course (args.courseId)
 */
const postCourseStudent = async (obj: any, args: any, context: any) => {
  await context.db.query(`replace into join_students_courses (courseId, studentId)
    values (${args.courseId}, ${args.studentId})`);
  return true;
}

/**
 * Get all the students of a course (obj.id). Response size can be limited (args.limit)
 */
const getCourseStudents = (obj: any, args: any, context: any) => {
  const limit = args.limit ? `limit ${args.limit}` : ``;
  return context.db.query(`select * from students as s left join join_students_courses as jsc on
    s.id = jsc.studentId where courseId = ${obj.id} ${limit}`);
}

/**
 * Delete a student (args.studentId) from a course (args.courseId)
 */
const deleteCourseStudent = async (obj: any, args: any, context: any) => {
  await context.db.query(`delete from join_students_courses where studentId = ${args.studentId} &&
    courseId = ${args.courseId}`);
  return true;
}

/**
 * Get the professor of a course, using the professor resolver
 */
const getCourseProfessor = async (obj: any, args: any, context: any) => {
  return (await getProfessors(obj, { id: obj.professorId }, context))[0];
}

export const courseResolvers = {
  Course: {
    students: getCourseStudents,
    professor: getCourseProfessor,
  },

  Query: {
    courses: getCourses,
  },

  Mutation: {
    putCourse,
    deleteCourse,
    postCourseStudent,
    deleteCourseStudent,
  }
};
