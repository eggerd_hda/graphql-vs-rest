import { print } from 'graphql';
import { makeExecutableSchema, mergeResolvers, mergeTypeDefs } from 'graphql-tools';
import { courseResolvers } from './resolver/course';
import { professorResolvers } from './resolver/professor';
import { studentResolvers } from './resolver/student';
import { courseType } from './schema/course';
import { professorType } from './schema/professor';
import { studentType } from './schema/student';

const rootTypes = `
  enum Department {
    Mathematik
    Informatik
    Maschinenbau
    Chemie
  }
`;

const typeDefs = mergeTypeDefs([
  rootTypes,
  studentType,
  professorType,
  courseType
], { throwOnConflict: true });

const resolvers = mergeResolvers([
  studentResolvers,
  professorResolvers,
  courseResolvers
]);

// print the merged schema
if (process.env.DEBUG_PRINT_SCHEMA === 'true') console.log(print(typeDefs));

export const schema = makeExecutableSchema({ typeDefs, resolvers });
